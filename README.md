This guide will help you configure Bitbucket Pipelines to automatically deploy a containerized application to Kubernetes. We'll create a deployment in Kubernetes to run multiple instances of our application, then package a new version of our Node.js app in a new version of a Docker image and push this image to DockerHub. Finally we'll update our deployment using Pipelines to release the new Docker image without a service outage.
Prerequisites
You'll need to have:
An existing DockerHub account (or other docker registry) to push the image to
An existing kubernetes cluster or minikube test environment
 
Build your artifact
In this example we package and deploy a Node.js app. To easily build the Node.js project you can configure your bitbucket-pipelines.yml to look like this: 
  BITBUCKET-PIPELINES.YML
  pipelines:
    default:
      - step:
          script: # Modify the commands below to build your repository.
            # build and test the Node app
            - npm install
            - npm test
 
